using System.Runtime.Serialization;

namespace Nutrimate.MealPlanner.Domain.Models
{
    public enum Goal
    {
        LoseWeight,
        SustainWeight,
        GainWeight
    }
}
