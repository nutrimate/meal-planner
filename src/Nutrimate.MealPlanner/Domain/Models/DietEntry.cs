namespace Nutrimate.MealPlanner.Domain.Models
{
    public class DietEntry
    {
        public DietEntry(ushort day, string recipeId, string mealTypeId)
        {
            Day = day;
            RecipeId = recipeId;
            MealTypeId = mealTypeId;
        }

        public ushort Day { get; set; }
        
        public string RecipeId { get; set; }
        
        public string MealTypeId { get; set; }
    }
}
