using System.Collections;
using System.Collections.Generic;

namespace Nutrimate.MealPlanner.Domain.Models
{
    public class Diet: IEnumerable<DietEntry>
    {
        private readonly IEnumerable<DietEntry> _entries;
        
        public Diet(IEnumerable<DietEntry> entries)
        {
            _entries = entries;
        }
        
        public IEnumerator<DietEntry> GetEnumerator()
        {
            return _entries.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _entries.GetEnumerator();
        }
    }
}
