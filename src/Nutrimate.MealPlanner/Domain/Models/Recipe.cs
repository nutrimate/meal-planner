using System.Collections.Generic;

namespace Nutrimate.MealPlanner.Domain.Models
{
    public class Recipe: IEqualityComparer<Recipe>
    {
        public Recipe(string id, IEnumerable<string> mealTypeIds, double calories, double proteins, double fats, double carbohydrates, IEnumerable<string> ingredients)
        {
            Id = id;
            MealTypeIds = mealTypeIds;
            Calories = calories;
            Proteins = proteins;
            Fats = fats;
            Carbohydrates = carbohydrates;
            Ingredients = ingredients;
        }

        public string Id { get; set; }
        
        public IEnumerable<string> MealTypeIds { get; set; }
        
        public double Calories { get; set; }
        
        public double Proteins { get; set; }
        
        public double Fats { get; set; }
        
        public double Carbohydrates { get; set; }
        
        public IEnumerable<string> Ingredients { get; set; }

        public bool Equals(Recipe? x, Recipe? y)
        {
            if (ReferenceEquals(x, y)) return true;
            if (ReferenceEquals(x, null)) return false;
            if (ReferenceEquals(y, null)) return false;
            if (x.GetType() != y.GetType()) return false;

            return x.Id == y.Id;
        }

        public int GetHashCode(Recipe obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}
