using System.Collections.Generic;
using Nutrimate.MealPlanner.Domain.Models;

namespace Nutrimate.MealPlanner.Domain.MealPlanning
{
    public class MealPlanningInput
    {
        public IEnumerable<string> MealSchedule { get; set; } = new List<string>();

        public double DailyCalorieIntake { get; set; }
        
        public double DailyMinProteins { get; set; }
        
        public double DailyMaxProteins { get; set; }
        
        public double DailyMinFats { get; set; }
        
        public ushort NumberOfDays { get; set; }
        
        public Goal Goal { get; set; }
    }
}
