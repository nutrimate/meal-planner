using System;
using Nutrimate.MealPlanner.Domain.Models;

namespace Nutrimate.MealPlanner.Domain.MealPlanning
{
    public class DietPlanningVariable: IComparable<DietPlanningVariable>
    {
        public DietPlanningVariable(ushort day, string mealTypeId, Recipe recipe)
        {
            Day = day;
            MealTypeId = mealTypeId;
            Recipe = recipe;
        }

        public ushort Day { get; }
        
        public string MealTypeId { get; }
        
        public Recipe Recipe { get; }

        public int CompareTo(DietPlanningVariable? other)
        {
            if (other == null)
            {
                return -1;
            }
            
            if (Day != other.Day)
            {
                return Day.CompareTo(other.Day);
            }

            if (MealTypeId != other.MealTypeId)
            {
                return string.Compare(MealTypeId, other.MealTypeId, StringComparison.Ordinal);
            }

            return string.Compare(Recipe.Id, other.Recipe.Id, StringComparison.Ordinal);
        }

        public override string ToString()
        {
            return $"Day={Day};MealTypeId={MealTypeId};RecipeId={Recipe.Id}";
        }
    }
}
