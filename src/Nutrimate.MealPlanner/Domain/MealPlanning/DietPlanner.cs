using System.Collections.Generic;
using System.Linq;
using Nutrimate.MealPlanner.Domain.ConstraintSolver;
using Nutrimate.MealPlanner.Domain.MealPlanning.Constraints;
using Nutrimate.MealPlanner.Domain.Models;

namespace Nutrimate.MealPlanner.Domain.MealPlanning
{
    public class DietPlanner
    {
        private readonly ConstraintSolver<DietPlanningVariable> _solver;

        public DietPlanner()
        {
            _solver = new ConstraintSolver<DietPlanningVariable>();
        }
        
        public Diet? PlanMeals(IEnumerable<Recipe> recipes, MealPlanningInput input)
        {
            var days = Enumerable.Range(1, input.NumberOfDays).Select(x => (ushort)x);
            var mealsWithRecipes = recipes.SelectMany(r =>
            {
                return r.MealTypeIds.Select(mt => new DietPlanningVariable(0, mt, r));
            });

            var variables = days.SelectMany(day =>
            {
                return mealsWithRecipes.Select(v => new DietPlanningVariable(day, v.MealTypeId, v.Recipe));
            });

            var constraints = new List<IModelConstraint<DietPlanningVariable>>
            {
                new MatchingMealScheduleConstraint(input.MealSchedule),
                new MatchingDailyCalorieIntakeConstraint(input.DailyCalorieIntake),
                new MatchingWeeklyCalorieIntakeConstraint(input.Goal, input.DailyCalorieIntake),
                new VariedDietConstraint(input.MealSchedule),
                new LimitFatsConstraint(input.DailyMinFats),
                new LimitProteinsConstraint(input.DailyMinProteins, input.DailyMaxProteins),
            };

            var solution = _solver.SolveProblem(variables, constraints);

            return solution != null ? PresentDietSolution(solution) : null;
        }

        private Diet PresentDietSolution(IEnumerable<DietPlanningVariable> solution)
        {
            var dietEntries = solution
                .Select(v => new DietEntry(v.Day, v.Recipe.Id, v.MealTypeId));
            
            return new Diet(dietEntries);
        }
    }
}
