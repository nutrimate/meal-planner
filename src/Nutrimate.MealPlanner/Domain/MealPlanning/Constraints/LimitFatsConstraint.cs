using System;
using System.Collections.Generic;
using System.Reflection.Emit;
using Nutrimate.MealPlanner.Domain.ConstraintSolver;
using Nutrimate.MealPlanner.Domain.MealPlanning.Constraints.Basic;
using Google.OrTools.Sat;

namespace Nutrimate.MealPlanner.Domain.MealPlanning.Constraints
{
    public class LimitFatsConstraint : IModelConstraint<DietPlanningVariable>
    {
        private readonly StaticConstraintBoundaries _boundaries;
        
        public LimitFatsConstraint(double minValue)
        {
            _boundaries = new StaticConstraintBoundaries
            {
                Min = (long) Math.Floor(minValue),
            };
        }

        public LinearExpr? Apply(CpModel model, IEnumerable<IVariablePair<DietPlanningVariable>> variables)
        {
            var constraint = new DailySumConstraint(v => (long) v.Recipe.Fats, _boundaries);

            constraint.Apply(model, variables);

            return null;
        }
    }
}
