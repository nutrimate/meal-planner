using System.Collections.Generic;
using System.Linq;
using Nutrimate.MealPlanner.Domain.ConstraintSolver;
using Nutrimate.MealPlanner.Domain.MealPlanning.Constraints.Basic;
using Nutrimate.MealPlanner.Domain.Models;
using Google.OrTools.Sat;

namespace Nutrimate.MealPlanner.Domain.MealPlanning.Constraints
{
    public class VariedDietConstraint : IModelConstraint<DietPlanningVariable>
    {
        private readonly IDictionary<string, int> _numberOfTimesMealsShouldAppearInADay;

        public VariedDietConstraint(IEnumerable<string> mealSchedule)
        {
            _numberOfTimesMealsShouldAppearInADay = mealSchedule.GroupBy(id => id)
                .ToDictionary(g => g.Key, g => g.Count());
            
        }

        public LinearExpr? Apply(CpModel model, IEnumerable<IVariablePair<DietPlanningVariable>> variables)
        {
            
            // let variablesGroupedByWeekAndRecipe =
            //     variables
            //     |> Map.toList
            //     |> groupByWeek
            //     |> Map.map (fun _ weeklyVariables ->
            //         weeklyVariables
            //     |> List.groupBy (fun ((_, _, r), _) -> r)
            //     |> Map.ofList)
            var variablesByWeek = variables.GroupByWeek();

            var errors = variablesByWeek
                .Select(v =>
                {
                    var weeklyVariables = v.ToList();

                    return TrackWeeklyRecipeVarianceError(model, weeklyVariables);
                });

            return LinearExpr.Sum(errors);
        }

        private LinearExpr TrackWeeklyRecipeVarianceError(CpModel model, IEnumerable<IVariablePair<DietPlanningVariable>> variables)
        {
            var errors = variables.GroupByRecipe()
                .Select(v =>
                {
                    var recipe = v.Key;
                    var weeklyVariablesForRecipe = v.ToList();

                    var numberOfTimesRecipeCanAppearInADay = recipe.MealTypeIds
                        .Select(GetNumberOfTimesMealShouldAppearInADay)
                        .Sum();

                    return AddRecipeAppearsOnlyOnceConstraintForWeek(
                        model,
                        recipe,
                        numberOfTimesRecipeCanAppearInADay,
                        weeklyVariablesForRecipe);
                });
                
            return LinearExpr.Sum(errors);
        }

        private int GetNumberOfTimesMealShouldAppearInADay(string meal)
        {
            return _numberOfTimesMealsShouldAppearInADay.ContainsKey(meal) ? _numberOfTimesMealsShouldAppearInADay[meal] : 0;
        }

        private LinearExpr AddRecipeAppearsOnlyOnceConstraintForWeek(
            CpModel model,
            Recipe recipe,
            int numberOfTimesRecipeCanAppear,
            ICollection<IVariablePair<DietPlanningVariable>> weeklyVariablesForRecipe)
        {
            var numberOfDays = weeklyVariablesForRecipe.Select(v => v.DomainVariable.Day).Distinct().Count();
            var maxError = numberOfDays * numberOfTimesRecipeCanAppear;

            var error = model.NewIntVar(0, maxError, $"Recipe: {recipe.Id} weekly appearance error");

            var modelVariables = weeklyVariablesForRecipe.Select(v => v.ModelVariable);
            var sum = LinearExpr.Sum(modelVariables);

            model.Add(new BoundedLinearExpression(0, sum - error, 1));

            return error;
        }
    }
}
