namespace Nutrimate.MealPlanner.Domain.MealPlanning.Constraints.Basic
{
    public class StaticConstraintBoundaries
    {
        public long Min { get; set; } = 0;
        
        public long Max { get; set; } = long.MaxValue;
    }
}
