using System;

namespace Nutrimate.MealPlanner.Domain.MealPlanning.Constraints.Basic
{
    public class DynamicConstraintBoundaries<T>
    {
        public Func<T, long> MinValueProvider { get; set; } = x => 0;

        public Func<T, long> MaxValueProvider { get; set; } = x => long.MaxValue;
    }
}
