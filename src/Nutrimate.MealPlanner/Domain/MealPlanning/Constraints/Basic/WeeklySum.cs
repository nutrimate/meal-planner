using Google.OrTools.Sat;

namespace Nutrimate.MealPlanner.Domain.MealPlanning.Constraints.Basic
{
    public class WeeklySum
    {
        public WeeklySum(int numberOfDays, LinearExpr sum)
        {
            NumberOfDays = numberOfDays;
            Sum = sum;
        }

        public int NumberOfDays { get; }
        
        public LinearExpr Sum { get; }
    }
}
