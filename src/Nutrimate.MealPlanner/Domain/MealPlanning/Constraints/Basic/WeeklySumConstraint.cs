using System;
using System.Collections.Generic;
using System.Linq;
using Nutrimate.MealPlanner.Domain.ConstraintSolver;
using Google.OrTools.Sat;

namespace Nutrimate.MealPlanner.Domain.MealPlanning.Constraints.Basic
{
    public class WeeklySumConstraint
    {
        private readonly Func<DietPlanningVariable, long> _valueProvider;
        private readonly DynamicConstraintBoundaries<int> _boundaries;
        
        public WeeklySumConstraint(Func<DietPlanningVariable, long> valueProvider, DynamicConstraintBoundaries<int> boundaries)
        {
            _valueProvider = valueProvider;
            _boundaries = boundaries;
        }
        
        public IEnumerable<WeeklySum> Apply(CpModel model, IEnumerable<IVariablePair<DietPlanningVariable>> variables)
        {
            var variablesByWeek = variables.GroupByWeek();

            return variablesByWeek
                .Select(group =>
                {
                    var variablesForWeek = group.ToList();

                    return AddSumConstraintForWeek(model, variablesForWeek);
                })
                .ToList();
        }

        private WeeklySum AddSumConstraintForWeek(CpModel model, ICollection<IVariablePair<DietPlanningVariable>> variables)
        {
            var numberOfDays = variables.Select(v => v.DomainVariable.Day).Distinct().Count();

            var modelVariables = variables.Select(v => v.ModelVariable);
            var values = variables.Select(v => _valueProvider(v.DomainVariable));
            var sum = LinearExpr.ScalProd(modelVariables, values);

            var minValue = _boundaries.MinValueProvider(numberOfDays);
            var maxValue = _boundaries.MaxValueProvider(numberOfDays);

            model.Add(new BoundedLinearExpression(minValue, sum, maxValue));

            return new WeeklySum(numberOfDays, sum);
        }
    }
}
