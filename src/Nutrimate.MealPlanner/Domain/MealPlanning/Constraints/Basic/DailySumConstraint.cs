using System;
using System.Collections.Generic;
using System.Linq;
using Nutrimate.MealPlanner.Domain.ConstraintSolver;
using Google.OrTools.Sat;

namespace Nutrimate.MealPlanner.Domain.MealPlanning.Constraints.Basic
{
    public class DailySumConstraint
    {
        private readonly Func<DietPlanningVariable, long> _valueProvider;
        private readonly StaticConstraintBoundaries _boundaries;

        public DailySumConstraint(Func<DietPlanningVariable, long> valueProvider, StaticConstraintBoundaries boundaries)
        {
            _valueProvider = valueProvider;
            _boundaries = boundaries;
        }

        public IEnumerable<LinearExpr> Apply(CpModel model, IEnumerable<IVariablePair<DietPlanningVariable>> variables)
        {
            var variablesByDay = variables.GroupByDay();

            return variablesByDay
                .Select(group =>
                {
                    var variablesForDay = group.ToList();

                    return AddSumConstraintForDay(model, variablesForDay);
                })
                .ToList();
        }

        private LinearExpr AddSumConstraintForDay(CpModel model, ICollection<IVariablePair<DietPlanningVariable>> variables)
        {
            var modelVariables = variables.Select(v => v.ModelVariable);
            var values = variables.Select(v => _valueProvider(v.DomainVariable));
            var sum = LinearExpr.ScalProd(modelVariables, values);

            model.Add(new BoundedLinearExpression(_boundaries.Min, sum, _boundaries.Max));

            return sum;
        }
    }
}
