using System.Collections.Generic;
using System.Linq;
using Nutrimate.MealPlanner.Domain.ConstraintSolver;
using Nutrimate.MealPlanner.Domain.Models;

namespace Nutrimate.MealPlanner.Domain.MealPlanning.Constraints.Basic
{
    public static class DietPlanningVariablesExtensions
    {
        public static IEnumerable<IGrouping<ushort, IVariablePair<DietPlanningVariable>>> GroupByDay(this IEnumerable<IVariablePair<DietPlanningVariable>> variables)
        {
            return variables
                .GroupBy(v => v.DomainVariable.Day);
        }

        public static IEnumerable<IGrouping<ushort, IVariablePair<DietPlanningVariable>>> GroupByWeek(this IEnumerable<IVariablePair<DietPlanningVariable>> variables)
        {
            return variables
                .GroupBy(v => (ushort)((v.DomainVariable.Day - 1) / 7));
        }
        
        public static IEnumerable<IGrouping<Recipe, IVariablePair<DietPlanningVariable>>> GroupByRecipe(this IEnumerable<IVariablePair<DietPlanningVariable>> variables)
        {
            return variables
                .GroupBy(v => v.DomainVariable.Recipe);
        }
    }
}
