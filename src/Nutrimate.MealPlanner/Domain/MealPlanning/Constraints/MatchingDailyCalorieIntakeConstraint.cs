using System;
using System.Collections.Generic;
using Nutrimate.MealPlanner.Domain.ConstraintSolver;
using Nutrimate.MealPlanner.Domain.MealPlanning.Constraints.Basic;
using Google.OrTools.Sat;

namespace Nutrimate.MealPlanner.Domain.MealPlanning.Constraints
{
    public class MatchingDailyCalorieIntakeConstraint : IModelConstraint<DietPlanningVariable>
    {
        private readonly StaticConstraintBoundaries _boundaries;
        
        public MatchingDailyCalorieIntakeConstraint(double dailyCalorieIntake)
        {
            const double minDailyCalorieIntakeFactor = 0.75;
            const double maxDailyCalorieIntakeFactor = 1.25;

            _boundaries = new StaticConstraintBoundaries
            {
                Min = (long) Math.Floor(dailyCalorieIntake * minDailyCalorieIntakeFactor),
                Max = (long) Math.Ceiling(dailyCalorieIntake * maxDailyCalorieIntakeFactor),
            };
        }

        public LinearExpr? Apply(CpModel model, IEnumerable<IVariablePair<DietPlanningVariable>> variables)
        {
            var constraint = new DailySumConstraint(v => (long) v.Recipe.Calories, _boundaries);

            constraint.Apply(model, variables);

            return null;
        }
    }
}
