using System.Collections.Generic;
using System.Linq;
using Nutrimate.MealPlanner.Domain.ConstraintSolver;
using Nutrimate.MealPlanner.Domain.MealPlanning.Constraints.Basic;
using Google.OrTools.Sat;

namespace Nutrimate.MealPlanner.Domain.MealPlanning.Constraints
{
    public class MatchingMealScheduleConstraint : IModelConstraint<DietPlanningVariable>
    {
        private readonly IDictionary<string, int> _mealTypeDailyFrequencies;
        
        public MatchingMealScheduleConstraint(IEnumerable<string> mealSchedule)
        {
            _mealTypeDailyFrequencies = mealSchedule
                .GroupBy(m => m)
                .ToDictionary(g => g.Key, g => g.Count());
        }
        
        public LinearExpr? Apply(CpModel model, IEnumerable<IVariablePair<DietPlanningVariable>> variables)
        {
            var variablesByDay = variables.GroupByDay();

            foreach (var dailyVariables in variablesByDay)
            {
                AddDailyMealScheduleConstraint(model, dailyVariables.ToList());
            }

            return null;
        }

        private void AddDailyMealScheduleConstraint(CpModel model, ICollection<IVariablePair<DietPlanningVariable>> variables)
        {
            var variablesGroupedByMealType = variables.GroupBy(v => v.DomainVariable.MealTypeId).ToList();
                
            // TODO: What about variables that have a meal type that doesn't exist in schedule
            foreach (var mealTypeFrequency in _mealTypeDailyFrequencies)
            {
                var variablesForMeal = variablesGroupedByMealType
                    .FirstOrDefault(g => g.Key == mealTypeFrequency.Key);

                var modelVariables =
                    variablesForMeal != null ? variablesForMeal.Select(v => v.ModelVariable) : new List<IntVar>();

                var sum = LinearExpr.Sum(modelVariables);

                model.Add(new BoundedLinearExpression(sum, mealTypeFrequency.Value, true));
            }
        }
    }
}
