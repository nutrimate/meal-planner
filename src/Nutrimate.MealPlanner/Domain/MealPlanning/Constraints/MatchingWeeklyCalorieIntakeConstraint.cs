using System;
using System.Collections.Generic;
using System.Linq;
using Nutrimate.MealPlanner.Domain.ConstraintSolver;
using Nutrimate.MealPlanner.Domain.MealPlanning.Constraints.Basic;
using Nutrimate.MealPlanner.Domain.Models;
using Google.OrTools.Sat;

namespace Nutrimate.MealPlanner.Domain.MealPlanning.Constraints
{
    public class MatchingWeeklyCalorieIntakeConstraint : IModelConstraint<DietPlanningVariable>
    {
        private readonly double _dailyCalorieIntake;
        private readonly DynamicConstraintBoundaries<int> _boundaries;
        
        public MatchingWeeklyCalorieIntakeConstraint(Goal goal, double dailyCalorieIntake)
        {
            var (minFactor, maxFactor) = GetMinAndMaxDietGoalCalorieFactors(goal);

            _dailyCalorieIntake = dailyCalorieIntake;
            _boundaries = new DynamicConstraintBoundaries<int>
            {
                MinValueProvider = days =>
                {
                    var weeklyCalorieIntake = dailyCalorieIntake * days;

                    return (long) Math.Floor(weeklyCalorieIntake * minFactor);
                },
                MaxValueProvider = days =>
                {
                    var weeklyCalorieIntake = dailyCalorieIntake * days;

                    return (long) Math.Ceiling(weeklyCalorieIntake * maxFactor);
                },
            };
        }

        public LinearExpr? Apply(CpModel model, IEnumerable<IVariablePair<DietPlanningVariable>> variables)
        {
            var constraint = new WeeklySumConstraint(v => (long) v.Recipe.Calories, _boundaries);

            var weeklySums = constraint.Apply(model, variables);

            var errors = weeklySums.Select((ws, i) => TrackWeeklySumError(model, i + 1, ws)); 

            return LinearExpr.Sum(errors);
        }
        
        private Tuple<double, double> GetMinAndMaxDietGoalCalorieFactors(Goal goal)
        {
            switch (goal)
            {
                case Goal.LoseWeight:
                    return new Tuple<double, double>(0.9, 1.0);
                case Goal.SustainWeight:
                    return new Tuple<double, double>(0.9, 1.1);
                case Goal.GainWeight:
                    return new Tuple<double, double>(1.0, 1.1);
                default:
                    throw new ArgumentOutOfRangeException(nameof(goal), goal, null);
            }
        }

        private LinearExpr TrackWeeklySumError(CpModel model, int weekNumber, WeeklySum weeklySum)
        {
            var numberOfDays = weeklySum.NumberOfDays;
            var sum = weeklySum.Sum;

            var weeklyTarget = (long) (numberOfDays * _dailyCalorieIntake);
            var error =
                model.NewIntVar(0, weeklyTarget, $"Week {weekNumber} Calorie Intake error");

            model.Add(new BoundedLinearExpression(weeklyTarget, sum + error, long.MaxValue));
            model.Add(new BoundedLinearExpression(long.MinValue, sum - error, weeklyTarget));

            return error;
        }
    }
}
