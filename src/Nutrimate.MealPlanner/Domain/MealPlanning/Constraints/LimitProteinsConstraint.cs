using System;
using System.Collections.Generic;
using Nutrimate.MealPlanner.Domain.ConstraintSolver;
using Nutrimate.MealPlanner.Domain.MealPlanning.Constraints.Basic;
using Google.OrTools.Sat;

namespace Nutrimate.MealPlanner.Domain.MealPlanning.Constraints
{
    public class LimitProteinsConstraint : IModelConstraint<DietPlanningVariable>
    {
        private readonly StaticConstraintBoundaries _boundaries;

        public LimitProteinsConstraint(double minValue, double maxValue)
        {
            _boundaries = new StaticConstraintBoundaries
            {
                Min = (long) Math.Floor(minValue),
                Max = (long) Math.Ceiling(maxValue)
            };
        }

        public LinearExpr? Apply(CpModel model, IEnumerable<IVariablePair<DietPlanningVariable>> variables)
        {
            var constraint = new DailySumConstraint(v => (long) v.Recipe.Proteins, _boundaries);

            constraint.Apply(model, variables);

            return null;
        }
    }
}
