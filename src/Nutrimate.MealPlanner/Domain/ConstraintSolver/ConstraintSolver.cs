using System;
using System.Collections.Generic;
using System.Linq;
using Google.OrTools.Sat;

namespace Nutrimate.MealPlanner.Domain.ConstraintSolver
{
    public class ConstraintSolver<T> where T: IComparable<T>
    {
        private readonly CpSolver _solver;

        public ConstraintSolver()
        {
            _solver = new CpSolver
            {
                StringParameters = "num_search_workers:4, max_time_in_seconds:25"
            };;
        }

        public IEnumerable<T>? SolveProblem(IEnumerable<T> variables, IEnumerable<IModelConstraint<T>> constraints)
        {
            var modelWithVariables = new ModelWithVariables<T>(variables);
            var model = modelWithVariables.Model;
            var variablePairs = modelWithVariables.VariablePairs;
            
            var errors = constraints.Select(c => c.Apply(model, variablePairs))
                .Where(exp => exp is { })
                .ToList();

            if (errors.Count > 0)
            {
                var objective = LinearExpr.Sum(errors);
                
                model.Minimize(objective);
            }

            var status = _solver.Solve(model);

            if (status != CpSolverStatus.Feasible && status != CpSolverStatus.Optimal)
            {
                // TODO: Return info why
                return null;
            }

            return GetSolution(variablePairs);
        }

        private IEnumerable<T> GetSolution(IEnumerable<IVariablePair<T>> variables)
        {
            return variables
                .Where(v => _solver.BooleanValue(v.ModelVariable))
                .Select(v => v.DomainVariable)
                .ToList();
        } 
    }
}
