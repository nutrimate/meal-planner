using System.Collections.Generic;
using Google.OrTools.Sat;

namespace Nutrimate.MealPlanner.Domain.ConstraintSolver
{
    public interface IVariablePair<T>
    {
        T DomainVariable { get; }
        IntVar ModelVariable { get; }
    }
}
