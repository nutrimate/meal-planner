using System;
using System.Collections.Generic;
using System.Linq;
using Google.OrTools.Sat;

namespace Nutrimate.MealPlanner.Domain.ConstraintSolver
{
    public class ModelWithVariables<T> where T: IComparable<T>
    {
        public CpModel Model { get; }

        public IEnumerable<IVariablePair<T>> VariablePairs { get; }
        
        public ModelWithVariables(IEnumerable<T> variables)
        {
            Model = new CpModel();
            VariablePairs = variables
                .Select(domainVariable =>
                {
                    var name = domainVariable.ToString() ?? "";
                    var modelVariable = Model.NewBoolVar(name);

                    IVariablePair<T> variablePair = new VariablePair<T>(domainVariable, modelVariable);
                    return variablePair;
                })
                .ToList();
        }
    }
}
