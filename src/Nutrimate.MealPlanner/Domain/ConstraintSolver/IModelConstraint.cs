using System;
using System.Collections.Generic;
using Google.OrTools.Sat;

namespace Nutrimate.MealPlanner.Domain.ConstraintSolver
{
    public interface IModelConstraint<T> where T: IComparable<T> 
    {
        LinearExpr? Apply(CpModel model, IEnumerable<IVariablePair<T>> variables);
    }
}
