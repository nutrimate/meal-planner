using Google.OrTools.Sat;

namespace Nutrimate.MealPlanner.Domain.ConstraintSolver
{
    public class VariablePair<T> : IVariablePair<T>
    {
        public VariablePair(T domainVariable, IntVar modelVariable)
        {
            DomainVariable = domainVariable;
            ModelVariable = modelVariable;
        }
        
        public T DomainVariable { get; }

        public IntVar ModelVariable { get; }
    }
}
