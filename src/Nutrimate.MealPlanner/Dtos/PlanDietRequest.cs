using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Nutrimate.MealPlanner.Domain.Models;

namespace Nutrimate.MealPlanner.Dtos
{
    public class PlanDietRequest
    {
        [Required]
        public IEnumerable<Recipe>? Recipes { get; set; }

        [Required]
        public IEnumerable<string>? MealSchedule { get; set; }
        
        [Required]
        public double DailyCalorieIntake { get; set; }
        
        [Required]
        public double DailyMinProteins { get; set; }
        
        [Required]
        public double DailyMaxProteins { get; set; }
        
        [Required]
        public double DailyMinFats { get; set; }
        
        [Required]
        [Range(1, 10)]
        public ushort Days { get; set; }
        
        [Required]
        public Goal Goal { get; set; }
    }
}
