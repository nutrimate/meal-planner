using System.Collections.Generic;
using Nutrimate.MealPlanner.Domain.Models;

namespace Nutrimate.MealPlanner.Dtos
{
    public class PlanDietResult
    {
        private PlanDietResult()
        {
        }

        public static PlanDietResult Success (Diet diet) 
        {
            return new PlanDietResult
            {
                Ok = true,
                Diet = diet
            };
        }

        public static PlanDietResult Failure (IEnumerable<string> errors)
        {
            return new PlanDietResult
            {
                Ok = false,
                Errors = errors
            };
        }

        public bool Ok { get; set; }
        
        public Diet? Diet { get; set; }

        public IEnumerable<string>? Errors { get; set; }
    }
}
