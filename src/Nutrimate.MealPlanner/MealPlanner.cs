﻿using System;
using System.Collections.Generic;
using Nutrimate.MealPlanner.Dtos;
using Nutrimate.MealPlanner.Domain.MealPlanning;

namespace Nutrimate.MealPlanner
{
    public class MealPlanner
    {
        public PlanDietResult PlanDiet(PlanDietRequest request)
        {
            var dietPlanner = new DietPlanner();
            
            var input = new MealPlanningInput
            {
                MealSchedule = request.MealSchedule ?? throw new ArgumentNullException(nameof(request.MealSchedule)),
                DailyCalorieIntake = request.DailyCalorieIntake,
                DailyMinProteins = request.DailyMinProteins,
                DailyMaxProteins = request.DailyMaxProteins,
                DailyMinFats = request.DailyMinFats,
                NumberOfDays = request.Days,
                Goal = request.Goal,
            };
            var recipes = request.Recipes ?? throw new ArgumentNullException(nameof(request.Recipes));
            
            var diet = dietPlanner.PlanMeals(recipes, input);
            if (diet == null)
            {
                var errors = new List<string> { "Failed to generate diet" };
                return PlanDietResult.Failure(errors);
            }

            return PlanDietResult.Success(diet);
        }
    }
}
