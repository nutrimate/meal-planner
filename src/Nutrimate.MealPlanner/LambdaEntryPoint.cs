using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Amazon.Lambda.Core;
using Nutrimate.MealPlanner.Dtos;
using Sentry;

[assembly: LambdaSerializer(typeof(Nutrimate.MealPlanner.JsonSerializer))]
namespace Nutrimate.MealPlanner
{
    public class LambdaEntryPoint
    {
        private MealPlanner mealPlanner = new MealPlanner();

        public PlanDietResult PlanDiet (PlanDietRequest request)
        {
            using (SentrySdk.Init())
            {
                var context = new ValidationContext(request, null, null);
                var results = new List<ValidationResult>();
                if (!Validator.TryValidateObject(request, context, results, true))
                {
                    var errors = results
                        .Where(r => r.ErrorMessage != null)
                        .Select(r => r.ErrorMessage ?? throw new ArgumentNullException());
                    
                    return PlanDietResult.Failure(errors);
                }

                return mealPlanner.PlanDiet(request);
            };
        }
    }
}
