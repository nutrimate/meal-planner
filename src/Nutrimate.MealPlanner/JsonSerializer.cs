using System.Text.Json;
using System.Text.Json.Serialization;
using Amazon.Lambda.Serialization.SystemTextJson;

namespace Nutrimate.MealPlanner
{
    public class JsonSerializer : DefaultLambdaJsonSerializer
    {
        public JsonSerializer() : base(CustomizeSerializerSettings)
        {
        }

        private static void CustomizeSerializerSettings(JsonSerializerOptions options)
        {
            options.Converters.Add(new JsonStringEnumConverter());
        }
    }
}
