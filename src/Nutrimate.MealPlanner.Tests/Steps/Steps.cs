using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nutrimate.MealPlanner.Domain.Models;
using TechTalk.SpecFlow;
using Nutrimate.MealPlanner.Tests.Drivers;
using FluentAssertions;
using TechTalk.SpecFlow.Assist;

namespace Nutrimate.MealPlanner.Tests.Steps
{
    [Binding]
    public class Steps
    {
        private readonly MealPlanningDriver _driver;

        public Steps(MealPlanningDriver driver)
        {
            _driver = driver;
        }

        [Given(@"following recipes")]
        public void GivenFollowingRecipes(Table table)
        {
            var recipes = table.CreateSet<StepRecipe>();

            _driver.Recipes = recipes
                .Select(r =>
                {
                    var mealTypeIds = new List<string> {r.Meal!};
                    return new Recipe(r.Name!, mealTypeIds, r.Calories, r.Protein, r.Fat, r.Carbohydrates, r.Ingredients ?? new List<string>());
                })
                .ToList();
        }
        
        [Given(
            @"an additional recipe (.*) for (.*) with (.*) calories, (.*) protein, (.*) fat, and (.*) carbohydrates")]
        public void GivenAnAdditionalRecipe(string name, string mealTypeId, double calories, double proteins,
            double fats, double carbohydrates)
        {
            var mealTypeIds = new List<string> {mealTypeId};
            var recipe = new Recipe(name, mealTypeIds, calories, proteins, fats, carbohydrates, new List<string>());

            _driver.Recipes.Add(recipe);
        }

        [Given(@"following daily meals")]
        public void GivenFollowingDailyMeals(Table table)
        {
            var meals = table.CreateSet<StepMeal>();
            _driver.MealTypeIds = meals.Select(m => m.Name!);
        }
        
        [Given(@"daily calorie intake of (.*)")]
        public void GivenDailyCalorieIntake(double dailyCalorieIntake)
        {
            _driver.DailyCalorieIntake = dailyCalorieIntake;
        }

        [Given(@"daily minimum fat intake of (.*)g")]
        public void GivenDailyMinimumFatIntake(double dailyMinimumFatIntake)
        {
            _driver.DailyMinFats = dailyMinimumFatIntake;
        }

        [Given(@"daily minimum protein intake of (.*)g")]
        public void GivenDailyMinimumProteinIntake(double dailyMinimumProteinIntake)
        {
            _driver.DailyMinProteins = dailyMinimumProteinIntake;
        }

        [Given(@"daily maximum protein intake of (.*)g")]
        public void GivenDailyMaximumProteinIntake(double dailyMaximumProteinIntake)
        {
            _driver.DailyMaxProteins = dailyMaximumProteinIntake;
        }

        [Given(@"my goal is to (.*)")]
        public void GivenMyGoalIs(Goal goal)
        {
            _driver.Goal = goal;
        }
        
        [When(@"I plan a diet for (\d+) days?")]
        public void WhenIPlanADietForXDays(ushort days)
        {
            _driver.PlanDiet(days);
        }

        [Then(@"the diet should look like this")]
        public void ThenDietShouldLookLikeThis(Table table)
        {
            var diet = table.CreateSet<StepDietEntry>()
                .Select(e =>
                {
                    e.Meal.Should().NotBeNull();
                    e.RecipeId.Should().NotBeNull();

                    return new DietEntry(e.Day,  e.RecipeId!, e.Meal!);
                });
            _driver.AssertGeneratedDietEquals(diet);
        }

        [Then(@"it should not propose a diet")]
        public void ThenShouldNotProposeADiet()
        {
            _driver.AssertDietWasNotGenerated();
        }
        
        [Then(@"it should propose a diet")]
        public void ThenShouldProposeADiet()
        {
            _driver.AssertDietWasGenerated();
        }

        [Then(@"the diet should include day that looks like following")]
        public void ThenDietShouldIncludeEntries(Table table)
        {
            var dietDays = table.CreateSet<ExpectedDietDay>();
            _driver.AssertDietIncludeDays(dietDays);
        }
    }
}
