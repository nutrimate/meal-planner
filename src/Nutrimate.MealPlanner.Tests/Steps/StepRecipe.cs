using System.Collections.Generic;

namespace Nutrimate.MealPlanner.Tests.Steps
{
    public class StepRecipe
    {
        public string? Name { get; set; }
        
        public string? Meal { get; set; }
        
        public double Calories { get; set; }
        
        public double Protein { get; set; }
        
        public double Fat { get; set; }
        
        public double Carbohydrates { get; set; }
        
        public IEnumerable<string>? Ingredients { get; set; }
    }
}
