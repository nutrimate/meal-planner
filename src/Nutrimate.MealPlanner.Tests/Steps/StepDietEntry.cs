namespace Nutrimate.MealPlanner.Tests.Steps
{
    public class StepDietEntry
    {
        public ushort Day { get; set; }
        
        public string? Meal { get; set; }
        
        public string? RecipeId { get; set; }
    }
}
