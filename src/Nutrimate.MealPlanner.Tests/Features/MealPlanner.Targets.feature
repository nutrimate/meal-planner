Feature: Diets match targets
  Diets should be balanced and match individual needs.

Background:
  Given following daily meals
  | Name      |
  | Breakfast |
  | Dinner    |
  | Supper    |
  And daily minimum fat intake of 48g
  And daily minimum protein intake of 48g
  And daily maximum protein intake of 144g
  And my goal is to sustain weight

Scenario: Should propose a diet for 1 day given perfect recipes
  Given following recipes
  | Name | Meal      | Calories | Protein | Fat | Carbohydrates |
  | A    | Breakfast | 600      | 40      | 20  | 50            |
  | B    | Dinner    | 600      | 40      | 20  | 50            |
  | C    | Supper    | 600      | 40      | 20  | 50            |
  And daily calorie intake of 1800
  When I plan a diet for 1 day
  Then the diet should look like this
  | Day | Meal      | Recipe id |
  | 1   | Breakfast | A         |
  | 1   | Dinner    | B         |
  | 1   | Supper    | C         |

Scenario: Should include all meals even when diet would match targets without one of them
  Given following recipes
  | Name | Meal      | Calories | Protein | Fat | Carbohydrates |
  | A    | Breakfast | 1        | 1       | 1   | 1             |
  | B    | Dinner    | 1800     | 120     | 60  | 150           |
  | C    | Supper    | 1        | 1       | 1   | 1             |
  And daily calorie intake of 1800
  When I plan a diet for 1 day
  Then the diet should look like this
  | Day | Meal      | Recipe id |
  | 1   | Breakfast | A         |
  | 1   | Dinner    | B         |
  | 1   | Supper    | C         |

Scenario: Should not return a diet if target meals cannot be reached
  Given following recipes
  | Name | Meal      | Calories | Protein | Fat | Carbohydrates |
  | A    | Breakfast | 600      | 40      | 20  | 50            |
  | B    | Dinner    | 600      | 40      | 20  | 50            |
  | C    | Supper    | 600      | 40      | 20  | 50            |
  And following daily meals
  | Name      |
  | Breakfast |
  | Lunch     |
  | Dinner    |
  | Supper    |
  And daily calorie intake of 1800
  When I plan a diet for 1 day
  Then it should not propose a diet

Scenario: Should not propose a diet when protein intake would be smaller than minimum
  Given following recipes
  | Name | Meal      | Calories | Protein | Fat | Carbohydrates |
  | A    | Breakfast | 500      | 25      | 40  | 50            |
  | B    | Dinner    | 500      | 25      | 40  | 50            |
  | C    | Supper    | 500      | 25      | 40  | 50            |
  And daily minimum fat intake of 80g
  And daily minimum protein intake of 80g
  And daily maximum protein intake of 240g
  And daily calorie intake of 1500
  When I plan a diet for 1 day
  Then it should not propose a diet

Scenario: Should not propose a diet when protein intake would be larger than maximum
  Given following recipes
  | Name | Meal      | Calories | Protein | Fat | Carbohydrates |
  | A    | Breakfast | 500      | 81      | 40  | 50            |
  | B    | Dinner    | 500      | 81      | 40  | 50            |
  | C    | Supper    | 500      | 81      | 40  | 50            |
  And daily minimum fat intake of 80g
  And daily minimum protein intake of 80g
  And daily maximum protein intake of 240g
  And daily calorie intake of 1500
  When I plan a diet for 1 day
  Then it should not propose a diet

Scenario: Should not propose a diet when fat intake would be smaller than minimum
  Given following recipes
  | Name | Meal      | Calories | Protein | Fat | Carbohydrates |
  | A    | Breakfast | 500      | 40      | 25  | 50            |
  | B    | Dinner    | 500      | 40      | 25  | 50            |
  | C    | Supper    | 500      | 40      | 25  | 50            |
  And daily minimum fat intake of 80g
  And daily minimum protein intake of 80g
  And daily maximum protein intake of 240g
  And daily calorie intake of 1500
  When I plan a diet for 1 day
  Then it should not propose a diet
