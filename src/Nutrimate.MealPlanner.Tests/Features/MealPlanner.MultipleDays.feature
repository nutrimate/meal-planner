Feature: Proposing diets for multiple days
  Diets proposed for multiple days should 

Background:
  Given following daily meals
    | Name      |
    | Breakfast |
    | Dinner    |
    | Supper    |
  And daily minimum fat intake of 48g
  And daily minimum protein intake of 48g
  And daily maximum protein intake of 144g
  And my goal is to sustain weight

Scenario: Should propose a monotonous diet for 7 days given 3 perfect recipes
  Given following recipes
  | Name | Meal      | Calories | Protein | Fat | Carbohydrates |
  | A    | Breakfast | 600      | 40      | 20  | 50            |
  | B    | Dinner    | 600      | 40      | 20  | 50            |
  | C    | Supper    | 600      | 40      | 20  | 50            |
  And daily calorie intake of 1800
  When I plan a diet for 7 days
  Then the diet should look like this
  | Day | Meal      | Recipe id |
  | 1   | Breakfast | A         |
  | 1   | Dinner    | B         |
  | 1   | Supper    | C         |
  | 2   | Breakfast | A         |
  | 2   | Dinner    | B         |
  | 2   | Supper    | C         |
  | 3   | Breakfast | A         |
  | 3   | Dinner    | B         |
  | 3   | Supper    | C         |
  | 4   | Breakfast | A         |
  | 4   | Dinner    | B         |
  | 4   | Supper    | C         |
  | 5   | Breakfast | A         |
  | 5   | Dinner    | B         |
  | 5   | Supper    | C         |
  | 6   | Breakfast | A         |
  | 6   | Dinner    | B         |
  | 6   | Supper    | C         |
  | 7   | Breakfast | A         |
  | 7   | Dinner    | B         |
  | 7   | Supper    | C         |

Scenario Outline: Should limit daily calorie intake between 90% and 110% when planning a diet for 1 day
  Given following recipes
  | Name | Meal   | Calories | Protein | Fat | Carbohydrates |
  | B    | Dinner | 600      | 40      | 20  | 50            |
  | C    | Supper | 600      | 40      | 20  | 50            |
  And an additional recipe A - small for Breakfast with <calories> calories, 40 protein, 50 fat, and 50 carbohydrates
  And daily calorie intake of 1800
  When I plan a diet for 1 days
  Then it <result> a diet

  Examples:
  | calories | result             |
  | 200      | should not propose |
  | 410      | should not propose |
  | 420      | should propose     |
  | 600      | should propose     |
  | 780      | should propose     |
  | 790      | should not propose |
  | 1000     | should not propose |

Scenario Outline: When sustaining weight should limit daily calorie intake between 75% and 125% while keeping total calorie intake between 90% and 110%
  Given following recipes
  | Name | Meal   | Calories | Protein | Fat | Carbohydrates |
  | B    | Dinner | 600      | 40      | 20  | 50            |
  | C    | Supper | 600      | 40      | 20  | 50            |
  And an additional recipe A - small for Breakfast with <small> calories, 40 protein, 50 fat, and 50 carbohydrates
  And an additional recipe A - large for Breakfast with <large> calories, 40 protein, 50 fat, and 50 carbohydrates
  And daily calorie intake of 1800
  And my goal is to sustain weight
  When I plan a diet for 2 days
  Then it <result> a diet

  Examples:
  | small | large | result             | comment    |
  | 75    | 75    | should not propose | -30%  -30% |
  | 75    | 600   | should propose     | -30%  + 0% |
  | 600   | 1150  | should propose     | + 0%  +30% |
  | 75    | 1150  | should not propose | -30%  +30% |
  | 240   | 240   | should not propose | -20%  -20% |
  | 240   | 960   | should propose     | -20%  +20% |
  | 200   | 1000  | should propose     | -25%  +25% |
  | 420   | 960   | should propose     | -10%  +20% |
  | 240   | 780   | should propose     | -20%  +10% |
  | 960   | 960   | should not propose | +20%  +20% |
  | 1150  | 1150  | should not propose | +30%  +30% |

Scenario Outline: When losing weight should limit daily calorie intake between 75% and 125% while keeping total calorie intake between 90% and 100%
  Given following recipes
  | Name | Meal   | Calories | Protein | Fat | Carbohydrates |
  | B    | Dinner | 600      | 40      | 20  | 50            |
  | C    | Supper | 600      | 40      | 20  | 50            |
  And an additional recipe A - small for Breakfast with <small> calories, 40 protein, 50 fat, and 50 carbohydrates
  And an additional recipe A - large for Breakfast with <large> calories, 40 protein, 50 fat, and 50 carbohydrates
  And daily calorie intake of 1800
  And my goal is to lose weight
  When I plan a diet for 2 days
  Then it <result> a diet

  Examples:
  | small | large | result             | comment    |
  | 75    | 75    | should not propose | -30%  -30% |
  | 75    | 600   | should propose     | -30%  + 0% |
  | 600   | 1150  | should propose     | + 0%  +30% |
  | 75    | 1150  | should not propose | -30%  +30% |
  | 240   | 240   | should not propose | -20%  -20% |
  | 240   | 960   | should propose     | -20%  +20% |
  | 200   | 1000  | should propose     | -25%  +25% |
  | 240   | 1000  | should not propose | -20%  +25% |
  | 240   | 780   | should propose     | -20%  +10% |
  | 960   | 960   | should not propose | +20%  +20% |
  | 1150  | 1150  | should not propose | +30%  +30% |

Scenario Outline: When gaining weight should limit daily calorie intake between 75% and 125% while keeping total calorie intake between 100% and 110%
  Given following recipes
  | Name | Meal   | Calories | Protein | Fat | Carbohydrates |
  | B    | Dinner | 600      | 40      | 20  | 50            |
  | C    | Supper | 600      | 40      | 20  | 50            |
  And an additional recipe A - small for Breakfast with <small> calories, 40 protein, 50 fat, and 50 carbohydrates
  And an additional recipe A - large for Breakfast with <large> calories, 40 protein, 50 fat, and 50 carbohydrates
  And daily calorie intake of 1800
  And my goal is to gain weight
  When I plan a diet for 2 days
  Then it <result> a diet

  Examples:
  | small | large | result             | comment    |
  | 75    | 75    | should not propose | -30%  -30% |
  | 75    | 600   | should propose     | -30%  + 0% |
  | 600   | 1150  | should propose     | + 0%  +30% |
  | 75    | 1150  | should not propose | -30%  +30% |
  | 240   | 240   | should not propose | -20%  -20% |
  | 240   | 960   | should propose     | -20%  +20% |
  | 200   | 1000  | should propose     | -25%  +25% |
  | 420   | 960   | should propose     | -10%  +20% |
  | 200   | 960   | should not propose | -25%  +20% |
  | 960   | 960   | should not propose | +20%  +20% |
  | 1150  | 1150  | should not propose | +30%  +30% |

Scenario: Should try to propose varied diet
  Given following recipes
  | Name | Meal      | Calories | Protein | Fat | Carbohydrates |
  | A    | Breakfast | 600      | 40      | 20  | 50            |
  | D    | Breakfast | 600      | 40      | 20  | 50            |
  | B    | Dinner    | 600      | 40      | 20  | 50            |
  | C    | Supper    | 600      | 40      | 20  | 50            |
  And daily calorie intake of 1800
  When I plan a diet for 2 days
  Then the diet should include day that looks like following
  | Meal      | Recipe id |
  | Breakfast | D         |
  | Dinner    | B         |
  | Supper    | C         |
  And the diet should include day that looks like following
  | Meal      | Recipe id |
  | Breakfast | A         |
  | Dinner    | B         |
  | Supper    | C         |

Scenario: Should prefer diet matching calorie target over varied diet not matching targets
  Given following recipes
  | Name | Meal      | Calories | Protein | Fat | Carbohydrates |
  | A    | Breakfast | 600      | 40      | 20  | 50            |
  | D    | Breakfast | 590      | 40      | 20  | 50            |
  | B    | Dinner    | 600      | 40      | 20  | 50            |
  | C    | Supper    | 600      | 40      | 20  | 50            |
  And daily calorie intake of 1800
  When I plan a diet for 2 days
  Then the diet should look like this
  | Day | Meal      | Recipe id |
  | 1   | Breakfast | A         |
  | 1   | Dinner    | B         |
  | 1   | Supper    | C         |
  | 2   | Breakfast | A         |
  | 2   | Dinner    | B         |
  | 2   | Supper    | C         |
