using System;
using System.Collections.Generic;
using Nutrimate.MealPlanner.Domain.Models;
using Nutrimate.MealPlanner.Domain.MealPlanning;
using FluentAssertions;

namespace Nutrimate.MealPlanner.Tests.Drivers
{
    public class MealPlanningDriver
    {
        public ICollection<Recipe> Recipes { get; set; } = new List<Recipe>();

        public IEnumerable<string> MealTypeIds { get; set; } = new List<string>();

        public double? DailyCalorieIntake { get; set; }

        public double? DailyMinProteins { get; set; }

        public double? DailyMaxProteins { get; set; }

        public double? DailyMinFats { get; set; }

        public Goal? Goal { get; set; }

        private Diet? _result;

        public void PlanDiet(ushort days)
        {
            if (DailyCalorieIntake == null)
            {
                throw new InvalidOperationException($"{nameof(DailyCalorieIntake)} is not set");    
            }
            
            if (DailyCalorieIntake == null)
            {
                throw new InvalidOperationException($"{nameof(DailyCalorieIntake)} is not set");    
            }

            if (DailyMinProteins == null)
            {
                throw new InvalidOperationException($"{nameof(DailyMinProteins)} is not set");
            }

            if (DailyMaxProteins == null)
            {
                throw new InvalidOperationException($"{nameof(DailyMaxProteins)} is not set");
            }
            
            if (DailyMinFats == null)
            {
                throw new InvalidOperationException($"{nameof(DailyMinFats)} is not set");
            }
            
            if (Goal == null)
            {
                throw new InvalidOperationException($"{nameof(Goal)} is not set");
            }

            var input = new MealPlanningInput
            {
                MealSchedule = MealTypeIds,
                DailyCalorieIntake = DailyCalorieIntake.Value,
                DailyMinProteins = DailyMinProteins.Value,
                DailyMaxProteins = DailyMaxProteins.Value,
                DailyMinFats = DailyMinFats.Value,
                NumberOfDays = days,
                Goal = Goal.Value,
            };

            var mealPlanner = new DietPlanner();

            _result = mealPlanner.PlanMeals(Recipes, input);
        }

        public void AssertGeneratedDietEquals(IEnumerable<DietEntry> diet)
        {
            _result.Should().BeEquivalentTo(diet);
        }

        public void AssertDietWasGenerated()
        {
            _result.Should().NotBeNull();
        }

        public void AssertDietWasNotGenerated()
        {
            _result.Should().BeNull();
        }

        public void AssertDietIncludeDays(IEnumerable<ExpectedDietDay> dietDays)
        {
            foreach (var dietDay in dietDays)
            {
                _result.Should().Contain(d => d.RecipeId == dietDay.RecipeId && d.MealTypeId == dietDay.Meal);
            }
        }
    }
}
