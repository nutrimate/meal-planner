namespace Nutrimate.MealPlanner.Tests.Drivers
{
    public class ExpectedDietDay
    {
        public string? Meal { get; set; }

        public string? RecipeId { get; set; }
    }
}
