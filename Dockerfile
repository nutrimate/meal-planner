FROM mcr.microsoft.com/dotnet/sdk:5.0-buster-slim as build

WORKDIR /build

COPY global.json meal-planner.sln ./
COPY ./src ./src

RUN dotnet build ./src/Nutrimate.MealPlanner -o ./build && \
  dotnet publish ./src/Nutrimate.MealPlanner -c Release  -o ./publish

FROM public.ecr.aws/lambda/dotnet:5.0

WORKDIR /var/task

COPY --from=build /build/publish .

CMD ["Nutrimate.MealPlanner::Nutrimate.MealPlanner.LambdaEntryPoint::PlanDiet"]